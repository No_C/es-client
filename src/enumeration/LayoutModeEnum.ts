/**
 * 布局方式
 */
enum LayoutModeEnum {

    /**
     * 默认
     */
    DEFAULT = 'default',

    /**
     * 经典
     */
    CLASSIC = 'classic'

}

export default LayoutModeEnum;