enum HttpTypeEnum {

    /**
     * 浏览器
     */
    BROWSER = 'browser',

    /**
     * 桌面客户端
     */
    DESKTOP = 'desktop',

    /**
     * 服务器模式
     */
    SERVER = 'server'

}

export default HttpTypeEnum;