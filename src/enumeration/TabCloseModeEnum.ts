enum TabCloseModeEnum {

    /**
     * 当达到上限会警告
     */
    ALERT = 1,

    /**
     * 关闭第一个
     */
    FIRST = 2

}

export default TabCloseModeEnum;