export default interface TabMenuItem {

    /**
     * ID
     */
    id: number;

    /**
     * 名称
     */
    name: string;

    /**
     * 关联ID
     */
    relationId?: number;

}