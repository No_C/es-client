import Field from "../Field";
import {IndexInfo, Mapping} from "@/es/IndexInfo";
import {Setting} from "@/es/IndexBase";
import {Index} from "@/es/Stats";

/**
 * 主页索引遍历
 */
export default interface IndexView {

    /**
     * 索引名称
     */
    name: string;

    /**
     * 别名
     */
    alias: Array<string>;

    /**
     * 映射
     */
    mapping: Mapping;

    /**
     * 设置
     */
    settings: Setting;

    /**
     * 字段
     */
    fields: Array<Field>;

    /**
     * 索引状态
     */
    indexStats: Index;

    /**
     * 索引元信息
     */
    indexInfo: IndexInfo;

    // ------------------------------------------- 渲染后 -------------------------------------------

    /**
     * 索引大小，美化后的
     */
    size: string;

    /**
     * 索引大小，原始逐句
     */
    original_size: number;

    /**
     * 文档数
     */
    doc_count: number;

    /**
     * 状态
     */
    state?: string;

    /**
     * 分片
     */
    shard: any;

    /**
     * 副本
     */
    replica: any;

}
