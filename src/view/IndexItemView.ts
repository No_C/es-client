import IndexView from "@/view/index/IndexView";

export default interface IndexItemView extends IndexView {

    /**
     * 是否展示
     */
    show: boolean;

}